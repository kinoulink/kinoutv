<?php include(dirname(__FILE__) . '/base.php'); ?><!DOCTYPE html>
<html>
    <head>
        <title>Kinoulink</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="google" value="notranslate">
        <script src="/jquery.js?v10" type="text/javascript"></script>
        <script src="/kinoulink.js?t=<?php echo time() ?>" type="text/javascript"></script>
        <script src="/moment.js" type="text/javascript"></script>
        <style>
            html, body {overflow:hidden;margin:0;padding:0;height:100%;}
            body {background:#111;}
            img {display:block;border:4px solid #fff;margin:auto}
            
            body.intro {background:#fff;background:url("http://www.kinoulink.com/img/body-color.png") repeat-x;}
            
            body.intro #image1 {max-height: 200px;}
            
            #intro {
                text-align: center;
                padding:2em;
                color:#333;
                font-family: Arial;
            }
            h2 {
                margin:2em;
            }
            
            #image1
            {
                background:white;
            }
            
            #image1.loading
            {
                background:white url("/loader.gif") no-repeat center center;
            }
            
            #message {
                background-color: #fff;
                background-image:url("/logo.png");
                background-repeat:no-repeat;
                background-position: right 50%;
                background-size: contain;
                padding: 10px;
                position: fixed;
                bottom: 0;
                font-size: 25px;
                left: 0;
                right: 0;
                text-align: center;
                font-family: Verdana;
            }
        </style>
    </head>
    <body class="intro">
        
        <div id="intro">
            <img src="/logo.png" style="margin:auto;margin-top:10px;width:200px;height:200px" />
            <h1>Bienvenue chez Kinoulink</h1>
            <h2>Partagez vos photos ...</h2>
            <h3>Votre diaporama va commencer dans <span id="startSeconds"></span> secondes...</h3>
            <img src="http://chart.apis.google.com/chart?chs=180x180&choe=UTF-8&cht=qr&chl=<?php echo TOKEN_PUBLIC ?>" />
            <h3>Votre code Kinoulink : <?php echo TOKEN_PUBLIC ?></h3>
        </div>
        
        <div id="message"></div>
        
    </body>
</html>
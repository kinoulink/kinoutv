<?php

    include(dirname(__FILE__) . '/base.php');
    
    $action = getParam('action');
    $currentMediaId = (int) getParam('media');
    
    $db = new DataBase();
    
    $sqlFields = 'id, media_id, views, date, title, author_name';
    
    $tableExist = $db->findOne('SELECT name FROM sqlite_master WHERE type="table" AND name="media"');
    
    if ($tableExist !== null && $action === 'next')
    {
        if ($currentMediaId != 0) {
            $currentMedia = $db->findOne('SELECT '.$sqlFields.' FROM media WHERE media_id = :media_id', array('media_id' => $currentMediaId));
        } else {
            $currentMedia = null;
        }
        
        $nextMedia = $db->findOne('SELECT '.$sqlFields.' FROM media WHERE views = 0 ORDER BY id LIMIT 1');
        
        if ($nextMedia === null && $currentMedia !== null)
        {
            $nextMedia = $db->findOne('SELECT '.$sqlFields.' FROM media WHERE id > :id ORDER BY id LIMIT 1', array('id' => $currentMedia['id']));
        }
        
        if ($nextMedia === null)
        {
            $nextMedia = $db->findOne('SELECT '.$sqlFields.' FROM media ORDER BY id LIMIT 1');
        }
        
        if ($nextMedia !== null)
        {
            $db->query('UPDATE media SET views = views + 1 WHERE id = :id', array('id' => $nextMedia['id']));
            
            renderJSON(array('media' => $nextMedia, 'success' => 1));
        }
    }
    
    renderJSON(array('success' => 0, 'error' => "Il n'y a pas encore de photo à afficher"));
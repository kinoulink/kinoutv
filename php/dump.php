<?php

    include(dirname(__FILE__) . '/base.php');
    
    $action = getParam('action');
    $currentMediaId = (int) getParam('media');
    
    $db = new DataBase();
    
    $sqlFields = 'id, media_id, views, date, title, author_name';

    $items = $db->find('SELECT '.$sqlFields.' FROM media ORDER BY id');
    
    foreach($items as $item)
    {
        echo join('  |  ', $item) . PHP_EOL;
    }


    
<?php

    include(dirname(__FILE__) . '/base.php');
    
    $uri = $_SERVER['REQUEST_URI'];
    
    $mediaId = filter_var($uri, FILTER_SANITIZE_NUMBER_INT);
    
    $mediaFilePath = DIR_MEDIA . '/' . $mediaId . '.jpg';
    
    $db = new DataBase();

    $media = $db->findOne('SELECT url FROM media WHERE media_id = :media_id', array('media_id' => $mediaId));
    
    if ($media === null)
    {
        exit(2);
    }

    $buffer = file_get_contents($media['url']);

	if (!empty($buffer))
	{
		file_put_contents($mediaFilePath, $buffer);
	}
    
    header('Content-Type: image/jpeg');
    die($buffer);

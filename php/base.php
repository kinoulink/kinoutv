<?php

    define('DIR_TMP', dirname(__FILE__) . '/../tmp');
    define('DIR_MEDIA', DIR_TMP . '/media');
    define('PATH_DATABASE', DIR_TMP . '/kinoulink.sqlite');
    define('PATH_TOKENS', DIR_TMP . '/tokens.php');
    
    define('BASE_URL', 'http://www.kinoulink.com/');
    
    include(dirname(__FILE__) . '/helper/common.php');
    include(dirname(__FILE__) . '/helper/database.php');

    if (!file_exists(DIR_TMP))
    {
        mkdir(DIR_TMP, 0755);
        mkdir(DIR_MEDIA, 0755);
        
        touch(PATH_DATABASE);
        
        $responseData = callApi('viewer/peering/create', array('type' => 3));
        
        if ($responseData === null || isset($responseData['error']))
        {
            die('<h1> Impossible de créer la Kinou !</h1>');
        }

        $data = $responseData['data'];

        $buffer = '<?php ' . PHP_EOL . 'define("TOKEN_SECURE", "'.$data['secure_token'].'");' . PHP_EOL . 'define("TOKEN_PUBLIC", "'.$data['public_token'].'");' . PHP_EOL;

        file_put_contents(PATH_TOKENS, $buffer);
        
        define("TOKEN_SECURE", $data['secure_token']);
        define("TOKEN_PUBLIC", $data['public_token']);
    }
    else
    {
        include(PATH_TOKENS);
    }
<?php
    
    function getParam($name)
    {
	return isset($_GET[$name]) ? $_GET[$name] : null;
    }
    
    function renderJSON($value)
    {
        die(json_encode($value));
    }
    
    function callApi($service, $data)
    {
        $ch = curl_init();
        
        $curlConfig = array(
            CURLOPT_URL            => BASE_URL . 'api/' . $service,
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => $data,
        );
        
        curl_setopt_array($ch, $curlConfig);
        
        $result = curl_exec($ch);
        
        curl_close($ch);

        return json_decode($result, 2);
      }
<?php

class DataBase
{            
    protected $pdoInstance;
        
    public function getConnection()
    {
        if ($this->pdoInstance === null)
        {            
            $this->pdoInstance = new \PDO('sqlite:' . PATH_DATABASE);
            
            $this->pdoInstance->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        }
        
        return $this->pdoInstance;
    }
    
    public function query($query = null, $params = array())
    {          
	foreach($params as $key => $value)
        {	    
	    if ($this->isSqlValue($value))
	    {
		$query = str_replace(':' . $key, $value['sql'], $query);
		
		unset($params[$key]);
	    }
        }
	
       $st = $this->__query($query, $params);
       
       return $st->rowCount();
    }
    
    protected function __query($query, $params = array())
    {
       try
       {
            $sth = $this->getConnection()->prepare($query);
            
            $sth->execute($params);            
       } 
       catch(\PDOException $e)
       {
           throw new \Exception($e->getMessage()."\n".$query.' : '.json_encode($params));
       }
       
       return $sth;
    }
    
    public function getValue($query, $params = array())
    {
        $res = $this->__query($query, $params)->fetch();
        
        return $res[0];
    }
        
    public function find($query, $params = array())
    {
        return $this->__query($query, $params)->fetchAll(\PDO::FETCH_ASSOC);
    }
        
    public function findOne($query, $params = array())
    {
        $r = $this->__query($query, $params)->fetch(\PDO::FETCH_ASSOC);
        
        return $r === false ? null : $r;
    }
    
    public function update($collection, $id, $data)
    {
        $sql = 'UPDATE '.$collection.' SET ';
        
        foreach($data as $key => $value)
        {
            $sql .= $key .'=:'.$key.', ';
        }
        
        $sql = trim($sql, ' ,');
        
        $sql .= ' WHERE id = :id LIMIT 1';
        
        $data['id'] = $id;
                
        return $this->query($sql, $data);
    }
    
    public function insert($collection, $data)
    {
        $sql = 'INSERT INTO '.$collection.' (';
        
        $values = '';
        
        foreach($data as $key => $value)
        {
            $sql .= $key.',';
	    
            $values .= ':'.$key.',';
        }
        
        $sql = trim($sql, ',');
        $values = trim($values, ',');
        
        $sql .= ') VALUES('.$values.')';
	
        $this->query($sql, $data);
        
        return $this->pdoInstance->lastInsertId();
    }
    
    public function delete($collection, $id)
    {
        return $this->query('DELETE FROM ' . $collection . ' WHERE id = :id LIMIT 1', array('id' => $id));
    }
    
    protected function isSqlValue($value)
    {
	return is_array($value) && isset($value['sql']);
    }
}
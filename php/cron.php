<?php

    include(dirname(__FILE__) . '/base.php');
    
    $newMediaItems = callApi('viewer/album/media/list', array('access_token' => TOKEN_SECURE));
        
    if ($newMediaItems !== null)
    {
        $db = new DataBase();
        
        $db->query('CREATE TABLE IF NOT EXISTS media (id INTEGER PRIMARY KEY, media_id INTEGER, views INTEGER, date INTEGER, url varchar(256), title varchar(256), author_name varchar(256), UNIQUE (media_id))');
        
        $existingMediaItems = $db->find('SELECT id, media_id, views FROM media ORDER BY id');
        
        $existingMediaItemsMap = array();
        
        foreach($existingMediaItems as $item)
        {
            $existingMediaItemsMap[$item['media_id']] = array('views' => $item['views']);
        }
        
        $db->query('DELETE FROM media');
        
        foreach($newMediaItems['data'] as $newMediaItem)
        {
            try 
            {
                if (isset($existingMediaItemsMap[$newMediaItem['id']]))
                {
                    $views = $existingMediaItemsMap[$newMediaItem['id']]['views'];
                }
                else
                {
                    $views = 0;
                }
                
                $db->insert('media', array(
                    'media_id'    => $newMediaItem['id'], 
                    'url'         => $newMediaItem['url'], 
                    'title'       => $newMediaItem['title'],
                    'date'        => $newMediaItem['date'],
                    'author_name' => $newMediaItem['author'],
                    'views'       => $views
                ));
                
                echo '+ ' . $newMediaItem['id'] . ' : ' . $newMediaItem['url'] . ' ' . $views . PHP_EOL;
                
            }
            catch(Exception $e) 
            {
                echo '<!> ' . $e->getMessage() . PHP_EOL;
            }
            
            //exec('cd "'.dirname(__FILE__).'/tmp";nohup wget "'.$newMediaItem['url'].'" -O "'.$newMediaItem['id'].'.jpg" --no-check-certificate --no-cookies > /dev/null 2>&1 & echo $!');
        }
    }
    else
    {
        die('Bad API response');
    }
#!/bin/bash

LOG=/tmp/kinoutv_xloader.log

xset s off                         # Don't activate screensaver
xset -dpms                         # Disable DPMS (Energy Star) features
xset s noblank                     # Don't blank the video device

[ -f ~/.config/chromium/Default/Preferences ] && sed -i 's/"exited_cleanly": false/"exited_cleanly": true/' ~/.config/chromium/Default/Preferences

sudo service nginx start

chromium-browser --kiosk http://127.0.0.1/
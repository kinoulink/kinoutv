#!/bin/bash

echo "Installing Kinou TV"

## Simple disk storage check. Naively assumes root partition holds all system data.
ROOT_AVAIL=$(df -k / | tail -n 1 | awk {'print $4'})
MIN_REQ="512000"

if [ $ROOT_AVAIL -lt $MIN_REQ ]; then
	echo "Insufficient disk space. Make sure you have at least 500MB available on the root partition."
	exit 1
fi

echo "Updating system package database..."
sudo apt-get update

echo "Upgrading the system..."
echo "(This might take a while.)"
sudo apt-get upgrade

echo "Remove SWAP"
sudo swapoff --all && sudo apt-get -y remove dphys-swapfile 

echo "Installing cool stuff..."
sudo apt-get -y install htop
sudo apt-get -y install nginx-light
sudo apt-get -y install php5 php5-fpm php5-cli php5-curl php5-sqlite

echo "Cloning KinouTV GIT project..."
git clone https://ebuildy@bitbucket.org/kinoulink/kinoutv.git
sudo chown www-data:www-data ./kinoutv -R

echo "Installing KinouTV on Nginx..."
sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
sudo ln -s ~/kinoutv/conf/nginx_prod.conf /etc/nginx/nginx.conf

sudo cp ~/kinoutv/script/cron /etc/cron.d/kinoulink
sudo chown root:root /etc/cron.d/kinoulink
[ -f /etc/cron.d/php5 ] && sudo unlink /etc/cron.d/php5

echo "Install Chromium.."
sudo apt-get -qq install chromium-browser

echo "Making modifications to X..."
[ -f ~/.config/openbox/lxde-rc.xml ] && mv ~/.config/openbox/lxde-rc.xml ~/.config/openbox/lxde-rc.xml.bak
[ -d ~/.config/openbox ] || mkdir -p ~/.config/openbox
ln -s ~/screenly/misc/lxde-rc.xml ~/.config/openbox/lxde-rc.xml
[ -f ~/.config/lxpanel/LXDE/panels/panel ] && mv ~/.config/lxpanel/LXDE/panels/panel ~/.config/lxpanel/LXDE/panels/panel.bak
[ -f /etc/xdg/lxsession/LXDE/autostart ] && sudo mv /etc/xdg/lxsession/LXDE/autostart /etc/xdg/lxsession/LXDE/autostart.bak
sudo sed -e 's/^#xserver-command=X$/xserver-command=X -nocursor/g' -i /etc/lightdm/lightdm.conf

echo "Adding KinouTV to X auto start..."
mkdir -p ~/.config/lxsession/LXDE/
echo "@~/kinoutv/script/xloader.sh" > ~/.config/lxsession/LXDE/autostart

echo "Quiet the boot process..."
sudo mv /boot/cmdline.txt /boot/cmdline.txt.bak
sudo cp ~/kinoutv/script/boot_cmdline.txt /boot/cmdline.txt

echo "Assuming no errors were encountered, you can now reboot and enjoy your KinouTV !"
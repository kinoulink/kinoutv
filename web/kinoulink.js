var currentMedia = null, isFirstCall = true, splashCounter = 10;

window.onerror = function(message, url, linenumber) 
{
    var img = document.createElement("img");
    var data = message;
    
    try {
        data = JSON.stringify({'source' : 'mobile', 'message' : message, 'url' : window.location.href, 'line' : linenumber, 'device' : navigator.userAgent});
    } catch(error) {}
    
    img.src = "//logs.loggly.com/inputs/c3ee4296-53d2-438a-b2ca-1ee9eaa0e6ea.gif?PLAINTEXT=" + encodeURIComponent(data) + "&DT=" + encodeURIComponent(Date());
};
            
$(function()
{
    var heightTotal = $(window).height() - 60;
    
    $('<img id="image1" />').appendTo("body").css({'display' : 'none', 'height' : 0.96 * heightTotal, 'margin-top' : 0.02 * heightTotal, 'max-width' : 0.9 * $(window).width(), 'min-width' : 200}).error(function()
    {
        $("#message").html("Erreur chargement " + $(this).attr('src'));
		
		setTimeout(function() {
			doProcess();
		}, 1000);

		$(this).removeClass("loading");
    });

    splashTick();
});

function splashTick()
{
    $("#startSeconds").html(splashCounter);

    splashCounter--;

    if (splashCounter === 0)
    {
        $("#startSeconds").html('quelques');
        
        doProcess();
    }
    else
    {
        setTimeout(function() {
            splashTick();
        }, 1000);
    }
}


function doProcess()
{                
    $.get('/api.php', {'action':'next', 'media' : currentMedia === null ? 0 : currentMedia.media_id}, function(response) 
    {
        if (response.success === 1) 
        {
            currentMedia = response.media;

            if (isFirstCall) 
            {
                isFirstCall = false;

                $("#intro").remove();
                $("body").removeClass('intro');
            }
            
            $("#image1").fadeOut(1000);

            setTimeout(function() 
            {
                $("#image1").attr('src', '/media/' + currentMedia.media_id + '.jpg').addClass("loading").fadeIn(1000).bind('load', function()
                {
                    setTimeout(function() {
                        doProcess();
                    }, 10000);
                    
                    $(this).unbind('load').removeClass("loading");
                });

                setMessage();
            }
            , 1000);
        }
        else if (response.hasOwnProperty('error'))
        {            
            $("#message").html(response.error);
            
            setTimeout(function() {
               doProcess();
            }, 3000);
        }
    }, 'json');
    
    return 'done';
}

function setMessage()
{
    $("#message").html('Photo de <strong>' + currentMedia.author_name + '</strong>');
                    
    if (currentMedia.title !== null && currentMedia.title.length > 0)
    {
        $("#message").append(' "' + currentMedia.title + '" ');
    }

    $("#message").append(" " + moment.unix(parseInt(currentMedia.date)).fromNow());
}